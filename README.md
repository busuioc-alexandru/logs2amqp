### Logs2Amqp

#### Environment variables
* `LOGS2AMQP_DIR` - Directory to scan for new log files
* `LOGS2AMQP_READ_OLD_LOGS` - `true` or whatever (`false`); makes sense when the logs are rotated; the default resolves to false
* `LOGS2AMQP_VERBOSITY` - One of `''` `v` `vv` or `vvv`; default `''`
* `AMQP_DSN` - The DSN to connect to the Amqp service
* `AMQP_QUEUE_NAME` - The queue name to send logs to
