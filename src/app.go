package main

import (
	"bufio"
	"context"
	"fmt"
	"github.com/fsnotify/fsnotify"
	"gitlab.com/busuioc-alexandru/rmq-client"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"sync"
	"time"
)

type app struct {
	ScanPath       string
	ReadOldLogs    bool
	amqpDsn        string
	amqpQueueName  string
	WorkInProgress sync.WaitGroup
	logger         *Logger
	rmqClient      *rmq.Client
}

var appInstance *app

func init() {
	appInstance = &app{
		ScanPath:      os.Getenv("LOGS2AMQP_DIR"),
		ReadOldLogs:   os.Getenv("LOGS2AMQP_READ_OLD_LOGS") == "true",
		amqpDsn:       os.Getenv("AMQP_DSN"), // amqp://guest:guest@localhost:5672/
		amqpQueueName: os.Getenv("AMQP_QUEUE_NAME"),
		logger:        NewLogger(log.New(os.Stderr, "", log.LstdFlags), Verbosity(os.Getenv("LOGS2AMQP_VERBOSITY"))),
	}
	appInstance.InitRmqClient()
}

func App() *app {
	return appInstance
}

func (a *app) Run(ctx context.Context) {
	a.WorkInProgress.Add(1)
	logsPath := a.ScanPath
	if logsPath == "" {
		a.logger.Println(Always, "no logs directory was specified in 'LOGS2AMQP_DIR' env. variable")
		os.Exit(1)
	}
	if f, err := os.Stat(logsPath); os.IsNotExist(err) {
		a.logger.Printf(Always, "The path does not exist: %s", err)
		os.Exit(2)
	} else if !f.IsDir() {
		a.logger.Printf(Always, "The path is not a directory: %s", err)
		os.Exit(3)
	}

	files, err := ioutil.ReadDir(logsPath)
	if err != nil {
		panic(err)
	}
	var singleFileReadWip sync.WaitGroup
	for _, file := range files {
		if !file.IsDir() {
			a.logger.Println(Verbose, file.Name()+" : parsing logs file")
			go a.readAndForward(file.Name(), logsPath, &singleFileReadWip, ctx)
		}
	}
	logsPathWatcher, err := fsnotify.NewWatcher()
	if err != nil {
		panic(err)
	}
	if err := logsPathWatcher.Add(logsPath); err != nil {
		panic(err)
	}
TheLoop:
	for {
		select {
		case event, ok := <-logsPathWatcher.Events:
			if !ok {
				continue
			}
			if event.Op&fsnotify.Create == fsnotify.Create {
				a.logger.Printf(Verbose2, "%s : the file was created; start watching\n", event.Name)
				fileBaseName := filepath.Base(event.Name)
				a.logger.Println(Verbose, fileBaseName+" : parsing logs file")
				go a.readAndForward(fileBaseName, logsPath, &singleFileReadWip, ctx)
			}
		case <-ctx.Done():
			a.logger.Println(Always, fmt.Errorf("closing the watch for new files"))
			break TheLoop
		}
	}
	singleFileReadWip.Wait()
	if err = a.rmqClient.Close(); err != nil {
		a.logger.Println(Always, err)
	}
	a.WorkInProgress.Done()
}

func (a *app) readAndForward(fileBaseName string, logsPath string, wg *sync.WaitGroup, ctx context.Context) {
	wg.Add(1)
	defer wg.Done()
	expr := regexp.MustCompile(`(\d{4})-(\d{2})-(\d{2})`)
	fileWithYmdName := expr.MatchString(fileBaseName) // file generated with logrotate, name like logs-2020-11-25.log
	var fileExpireDate time.Time
	if fileWithYmdName {
		date := expr.FindAllStringSubmatch(fileBaseName, -1)
		year, err := strconv.Atoi(date[0][1])
		month, _ := strconv.Atoi(date[0][2])
		day, _ := strconv.Atoi(date[0][3])
		if err != nil {
			a.logger.Printf(Always, "error parsing the year from file name (expected format is YYYY-MM-DD)")
			fileExpireDate = time.Now().Add(time.Hour * 24).Truncate(time.Hour * 24) // fallback
		} else {
			fileExpireDate = time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC).Add(time.Hour * 24)
		}
		a.logger.Printf(Verbose2, "%s : file expire date was parsed as %s", fileBaseName, fileExpireDate.String())
		if logFileIsOld(fileExpireDate) && !a.ReadOldLogs {
			a.logger.Println(Verbose, fmt.Sprintf("%s : the file is old; do not read old logs", fileBaseName))
			return // do not read this file
		}
	}
	filePath := filepath.Join(logsPath, fileBaseName)
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		panic(err)
	}
	//goland:noinspection GoUnhandledErrorResult
	defer watcher.Close()
	if err := watcher.Add(filePath); err != nil {
		a.logger.Println(Always, fmt.Errorf("ERROR: cannot add ["+fileBaseName+"] file to fswatch: %v", err))
	}
	f, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	//goland:noinspection GoUnhandledErrorResult
	defer f.Close()
	reader := bufio.NewReader(f)
	recordsCounter := &RecordsCounter{}
ReadLineByLine:
	for {
		line, err := reader.ReadBytes('\n')
		if err == nil {
			// send logs to amqp/rabbitmq
			if err = a.publishMessage(line); err != nil {
				a.logger.Printf(Verbose, "%s : error sending to amqp: %s\n", fileBaseName, err)
			} else {
				recordsCounter.Add(1)
				a.logger.Printf(Verbose3, "%s : %s\n", fileBaseName, string(line)[:101]+"...")
			}
		} else if err == io.EOF {
			a.logger.Printf(Verbose2, "%s : EOF reached\n", fileBaseName)
			if recordsCount := recordsCounter.Flush(); recordsCount > 0 {
				a.logger.Println(Verbose, fmt.Sprintf("%s : %d records were sent till EOL", fileBaseName, recordsCount))
			}
			if fileWithYmdName && logFileIsOld(fileExpireDate) {
				a.logger.Printf(Verbose, "%s : the file is old; stop watching\n", fileBaseName)
				return // stop watching this file
			}
			a.logger.Printf(Verbose2, "%s : wait for updates / ping in 1 min.\n", fileBaseName)
			for {
				select {
				case <-time.After(time.Minute):
					a.logger.Printf(Verbose2, "%s : resume\n", fileBaseName)
					continue ReadLineByLine
				case event, ok := <-watcher.Events:
					if !ok {
						continue
					}
					a.logger.Println(Verbose3, "event:", event)
					if event.Op&fsnotify.Write == fsnotify.Write {
						a.logger.Println(Verbose2, "modified file:", event.Name)
						time.Sleep(time.Second)
						continue ReadLineByLine
					}
					if event.Op&fsnotify.Remove == fsnotify.Remove {
						a.logger.Printf(Verbose, "%s : the file was removed; stop watching\n", fileBaseName)
						return // stop watching this file
					}
				case err, ok := <-watcher.Errors:
					if !ok {
						continue
					}
					a.logger.Println(Always, "error:", err)
				case <-ctx.Done():
					a.logger.Println(Always, fmt.Errorf("stopping the watch for the [%s] file", fileBaseName))
					return
				}
			}
		} else {
			panic(err)
		}
	}
}

func logFileIsOld(date time.Time) bool {
	return time.Now().After(date)
}

func (a *app) publishMessage(message []byte) error {
	return a.rmqClient.Push(message, rmq.PushContentTypeApplicationJson)
}

func (a *app) InitRmqClient() {
	a.rmqClient = rmq.New(a.amqpQueueName, a.amqpDsn, nil, a.logger.GetLoggerIo())
}
