package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	terminateSignal := make(chan os.Signal, 1)
	signal.Notify(terminateSignal, syscall.SIGINT, syscall.SIGTERM)
	cancelCtx, cancelFn := context.WithCancel(context.Background())
	app := App()
	go app.Run(cancelCtx)
	select {
	case <-terminateSignal:
		log.Println("exit signal received")
		cancelFn()
		log.Println("the worker was notified to stop processing")
		app.WorkInProgress.Wait()
		log.Println("ready to shutdown")
	}
}
