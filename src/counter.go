package main

import "sync"

type RecordsCounter struct {
	m     sync.Mutex
	count int
}

func (c *RecordsCounter) Add(n int) {
	c.m.Lock()
	defer c.m.Unlock()
	c.count += n
}

func (c *RecordsCounter) Flush() int {
	c.m.Lock()
	defer c.m.Unlock()
	count := c.count
	c.count = 0
	return count
}
