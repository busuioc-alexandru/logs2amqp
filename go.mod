module go-logs2amqp

go 1.15

require (
	github.com/fsnotify/fsnotify v1.4.9
	gitlab.com/busuioc-alexandru/rmq-client v0.5.1
)
